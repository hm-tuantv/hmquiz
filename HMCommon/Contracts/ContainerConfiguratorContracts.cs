﻿using HMCommon.Injection;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics.Contracts;

namespace HMCommon.Contracts
{
    [ContractClassFor(typeof(IContainerConfigurator))]
    abstract class ContainerConfiguratorContracts : IContainerConfigurator
    {
        public void Configure(IServiceCollection services)
        {
            Contract.Requires(services != null);
        }
    }
}
