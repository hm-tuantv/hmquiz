﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;

namespace HMCommon.Configuration
{
    public class TokenConfiguration
    {
        public string Issuer { get; set; }

        public string Audience { get; set; }

        public string SecretKey { get; set; }

        public TimeSpan Expiration => TimeSpan.FromMinutes(ExpirationInMinutes);

        public SigningCredentials SigningCredentials => new SigningCredentials(SecurityKey, SecurityAlgorithms.HmacSha256);

        public SymmetricSecurityKey SecurityKey => new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));
        public int ExpirationInMinutes { get; set; }
    }
}
