﻿using HMCommon.Contracts;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics.Contracts;

namespace HMCommon.Injection
{
    [ContractClass(typeof(ContainerConfiguratorContracts))]
    public interface IContainerConfigurator
    {
        void Configure(IServiceCollection services);
    }
}
