﻿using Microsoft.AspNetCore.Identity;

namespace HMQuiz.Data.Model
{
    public class User //: IdentityUser
    {
        public string Email { get; set; }
    }
}
