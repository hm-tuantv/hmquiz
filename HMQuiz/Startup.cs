﻿using HMCommon.Configuration;
using HMCommon.Injection;
using HMQuiz.Data;
using HMQuiz.Data.Injection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace HMQuiz
{
    public partial class Startup
    {
        public static readonly IList<IContainerConfigurator> Configurators = new List<IContainerConfigurator>
        {
            new DataRepositoryContainerConfigurator(),
           
        }.AsReadOnly();
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                 .SetBasePath(env.ContentRootPath)
                 .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                 .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                 .AddEnvironmentVariables();
            Configuration = builder.Build();
            // CurrentEnvironment = env;
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Register all depencency
            services.ConfigureDepencencyInjection(Configuration, Configurators);
            // Add framework services.
            //services.AddDbContext<HMQuizContext>(options =>
            //    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")))
            ConfigureAuthServices(services);
            services.AddMvc();
            services.AddCors();
            ConfigureAppSettings(services);
            //for development purpose
            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes("Bearer")
                    .RequireAuthenticatedUser()
                    .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            })
                   .AddMvcOptions(opts =>
                   {
                     //  opts.Filters.Add(typeof(GlobalExceptionFilter));
                      // opts.ModelBinderProviders.Add(new EnumTypeModelBinderProvider<SensorType>());
                      // opts.ModelBinderProviders.Add(new EnumTypeModelBinderProvider<SensorStatus>());
                   })
                   .AddJsonOptions(opts =>
                   {
                        // Force Camel Case to JSON
                        opts.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                   });

            // Register the Swagger generator, defining one or more Swagger documents
            ConfigureSwaggerServices(services);
            services.Configure<DbConfiguration>(options =>
            {
                options.ConnectionString
                    = Configuration.GetSection("MongoConnection:ConnectionString").Value;
                options.Database
                    = Configuration.GetSection("MongoConnection:Database").Value;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
            IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            ConfigureAuth(app);
            app.UseMvc();
            ConfigureSwagger(app);
        }
        private void ConfigureAppSettings(IServiceCollection services)
        {
            services.Configure<TokenConfiguration>(Configuration.GetSection("JwtIssuerOptions"));           
            //@todo
            // services.AddScoped<IpRestrictionAttribute>();
        }
    }
}
