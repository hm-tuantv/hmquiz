﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace HMQuiz
{
    public partial class Startup
    {
        public void ConfigureSwagger(IApplicationBuilder app)
        {
            app.UseSwagger()
                .UseSwaggerUI(c =>
                {
                    c.EnabledValidator();
                    c.SwaggerEndpoint("v1/swagger.json", "KBR API V1");
                    c.ShowRequestHeaders();
                    c.ShowJsonEditor();
                });
        }
        public void ConfigureSwaggerServices(IServiceCollection services)
        {            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "KBR API", Version = "v1" });
              //  c.OperationFilter<TokenOperationFilter>();
               // c.OperationFilter<SwaggerFileOperationFilter>();
               // c.OperationFilter<FormFileOperationFilter>();
                //MethodHelpers.CallGenericMethodForImplementingTypes(typeof(EnumType<>), typeof(Startup), null,
                //    nameof(MapEnumType), BindingFlags.NonPublic | BindingFlags.Static, new object[] { c });

            });

        }
    }
}
