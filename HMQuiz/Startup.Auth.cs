﻿using HMCommon.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HMQuiz
{
    public partial class Startup
    {
        public void ConfigureAuth(IApplicationBuilder app)
        {
            app.UseAuthentication();
        }
        public void ConfigureAuthServices(IServiceCollection services)
        {
            var tokenOptions = new TokenConfiguration();
            Configuration.GetSection("JwtIssuerOptions").Bind(tokenOptions);

            var tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = tokenOptions.SecurityKey,
                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = true,
                ValidIssuer = tokenOptions.Issuer,
                // Validate the JWT Audience (aud) claim
                ValidateAudience = true,
                ValidAudience = tokenOptions.Audience,
                // Validate the token expiry
                ValidateLifetime = true,
                // If you want to allow a certain amount of clock drift, set that here:
                ClockSkew = TimeSpan.Zero
            };

            services.AddAuthentication()
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = tokenValidationParameters;
                });
            //services.AddIdentity<User, Role>(config =>
            //{
            //    //     Password settings
            //    //    config.Password.RequireDigit = true;
            //    config.Password.RequiredLength = 8;
            //    //    config.Password.RequireNonAlphanumeric = false;
            //    //    config.Password.RequireUppercase = true;
            //    //    config.Password.RequireLowercase = false;

            //    //    // Lockout settings
            //    //    config.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
            //    //    config.Lockout.MaxFailedAccessAttempts = 10;

            //    //    // User settings
            //    config.User.RequireUniqueEmail = true;
            //})
               // .AddEntityFrameworkStores<KBRContext>()
               // .AddDefaultTokenProviders();
        }
    }
}
