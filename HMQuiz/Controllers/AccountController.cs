﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using HMCommon.Configuration;
using HMCommon.Contracts;
using HMQuiz.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace HMQuiz.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly TokenConfiguration _options;
        private readonly IUserRepository _userRepository;
        public AccountController(IOptions<TokenConfiguration> options,
            IUserRepository repository
            )
        {
            _userRepository = ContractHelper.RequiresNotNull(repository);
            _options = ContractHelper.RequiresNotNull(options).Value;
        }
        /// <summary>
        /// Login
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Token for successful login</returns>
        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<object> LoginAsync(string usrName, string password)
        {
            //if (!ModelState.IsValid)
            //{
            //    return ModelState.AsWebServiceResult<TokenWebObject>();
            //}
            //var userServiceResult = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, false, false);
            //if (!userServiceResult.Succeeded)
            //    return new WebServiceResult<TokenWebObject>
            //    {
            //        WebErrors = new List<WebError>
            //        {
            //            WebError.UserIsNotValid
            //        }
            //    };

            //var usr = (await _userManager.FindByNameAsync(model.UserName));
            //if (usr != null && usr.Status == UserStatus.InActive.Name)
            //{
            //    return new WebServiceResult<TokenWebObject>
            //    {
            //        WebErrors = new List<WebError>
            //        {
            //            WebError.UserInactived
            //        }
            //    };
            //}
            await _userRepository.AddUser(new Data.Model.User
            {
                Email = "tuantv@gmail.com"
            });
            var tmp = _userRepository.GetUserById("123");
            var fullName = "abc";// (await _userManager.FindByNameAsync(model.UserName)).FullName;
            var role = "admin";// (await GetRoleByUserName(usr.UserName));
            var refreshToken = Guid.NewGuid().ToString().Replace("-", "");
            var token = GetJwt("app", refreshToken, role, "tuantv", fullName, "123"); //GetJwt(model.ClientId, refreshToken, role, usr.UserName, fullName, usr.Id);
            //var tokenBusiness = new RefreshToken
            //{
            //    ProtectedTicket = refreshToken,
            //    ClientId = model.ClientId,
            //    Id = Guid.NewGuid().ToString(),
            //    IssuedUtc = token.Result.IssuedDate,
            //    ExpiresUtc = token.Result.IssuedDate.AddSeconds(token.Result.ExpiryIn),
            //    UserId = usr.Id,
            //};
            //await userRepository.AddToken(tokenBusiness);
            return token;
        }
        #region Helpers       
        private object GetJwt(string clientId,
            string refreshToken,
            string role,
            string userName = "",
            string fullName = "",
            string usrId = "")
        {
            var now = DateTime.UtcNow;
            var expiryTime = now.Add(TimeSpan.FromMinutes(_options.ExpirationInMinutes));

            var claims = new[]
            {
                new Claim(ClaimTypes.UserData, clientId),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, role.ToUpper() ?? string.Empty),
                new Claim(JwtRegisteredClaimNames.Iat,
                    new DateTimeOffset(now)
                        .ToUniversalTime()
                        .ToUnixTimeMilliseconds()
                        .ToString(),
                    ClaimValueTypes.Integer64),
                new Claim(ClaimTypes.Name, userName),
                new Claim(ClaimTypes.Expiration,  new DateTimeOffset(expiryTime)
                        .ToUniversalTime()
                        .ToUnixTimeMilliseconds()
                        .ToString()),
                  new Claim(ClaimTypes.GivenName, fullName),
                new Claim(ClaimTypes.NameIdentifier, usrId)
            };
            // Create the JWT and write it to a string
            var jwt = new JwtSecurityToken(
                issuer: _options.Issuer,
                audience: _options.Audience,
                claims: claims,
                notBefore: now,
                expires: expiryTime,
                signingCredentials: _options.SigningCredentials);
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                Token = encodedJwt,
                Issuer = _options.Issuer,
                IssuedDate = new DateTimeOffset(now),
                ExpiryIn = _options.ExpirationInMinutes * 60,
                TokenType = "bearer",
                RefreshToken = refreshToken,
                ClientId = clientId,
                Role = role.ToUpper()
            };

            return response;
        }
        #endregion
    }
}