﻿using System;
using System.Threading.Tasks;
using HMCommon.Configuration;
using HMQuiz.Data.Model;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace HMQuiz.Data.Mongodb.Impl
{
    public class UserRepository : IUserRepository
    {
        private readonly HMQuizContext _context = null;

        public UserRepository(IOptions<DbConfiguration> settings)
        {
            _context = new HMQuizContext(settings);
        }

        public async Task AddUser(User item)
        {
            try
            {
                await _context.Users.InsertOneAsync(item);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public User GetUserById(string userId)
        {
            try
            {
                return _context.Users
                .AsQueryable()
                .FirstOrDefault();
            }
            catch (System.Exception e)
            {

                throw;
            }
            

        }
    }
}
