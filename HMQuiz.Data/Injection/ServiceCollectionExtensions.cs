﻿using HMCommon.Injection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace HMQuiz.Data.Injection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection ConfigureDepencencyInjection(
            this IServiceCollection services,
            IConfigurationRoot config,
            IEnumerable<IContainerConfigurator> configurators)
        {
            foreach (var configurator in configurators)
            {
                configurator.Configure(services);
            }

            return services;
        }

    }
}
