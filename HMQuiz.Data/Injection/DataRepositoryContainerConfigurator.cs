﻿using HMCommon.Injection;
using HMQuiz.Data.Mongodb.Impl;
using Microsoft.Extensions.DependencyInjection;

namespace HMQuiz.Data.Injection
{
    public class DataRepositoryContainerConfigurator : IContainerConfigurator
    {
        public void Configure(IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
        }
    }
}
