﻿using HMCommon.Configuration;
using HMQuiz.Data.Model;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace HMQuiz.Data
{
    public class HMQuizContext //: IdentityDbContext<User, Role, string>
    {
        protected static IMongoClient _client;
        protected static IMongoDatabase _database;
        public HMQuizContext(IOptions<DbConfiguration> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
        }
        public IMongoCollection<User> Users
        {
            get
            {
                return _database.GetCollection<User>("User");
            }
        }
    }
}
