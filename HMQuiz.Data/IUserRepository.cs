﻿using HMQuiz.Data.Model;
using System.Threading.Tasks;

namespace HMQuiz.Data
{
    public interface IUserRepository
    {
        User GetUserById(string userId);
        Task AddUser(User item);
    }
}
